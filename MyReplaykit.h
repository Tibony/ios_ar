Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//add start
#include <string>
using namespace std;
#if PLATFORM_IOS
#import <UIKit/UIKit.h>
#import <ReplayKit/ReplayKit.h>
#import <AVKit/AVKit.h>


#import "IOS/IOSAppDelegate.h"
#import "IOS/IOSView.h"

@class SlateOpenGLESViewController;

@interface ScreenshotEncoder : UIViewController <UINavigationControllerDelegate>
@end


@interface ShareItem : NSObject

-(instancetype)initWithData:(UIImage *)img andFile:(NSURL *)file;

@property (nonatomic, strong) UIImage *img;
@property (nonatomic, strong) NSURL *path;

@end

@interface TViewController : UIViewController{

    int mycount;
    bool shotModel;

}
@property (nonatomic, strong)AVAssetWriterInput *videoInput;
@property (nonatomic, strong)AVAssetWriterInput *audioInput;
@property (nonatomic, strong)AVAssetWriterInput *micInput;
@property (nonatomic, strong)AVAssetWriter *assetWriter;
@property (nonatomic, strong)NSURL *filePath;
@property (nonatomic, strong)NSString *filePathString;
@property (nonatomic, strong)NSString *filename;




@end
@interface TVideoPlay : UIViewController{

}
@property(strong,nonatomic)AVPlayer *VideoPlayer;
@property(strong,nonatomic)AVPlayerItem *VideoItem;
@property(strong,nonatomic)AVPlayerLayer *PlayerLayer;
@property(strong,nonatomic)UIButton* Button_Play;
@property (nonatomic, strong)NSString *GetfilePathString;
@property (nonatomic, strong)NSString *Getfilename;
@property (nonatomic, strong)UIButton* Button_pause;
@property bool shotModel;
@end


#endif

