//
//  MySelfIAPObject.h
//  MySelf
//
//  Created by MySelf on 2018/7/13.
//  Copyright © 2018年 MySelf. All rights reserved.
//  苹果内购单例类
#if PLATFORM_IOS
#import <Foundation/Foundation.h>

@interface MySelfIAPObject : NSObject

/**
 单利创建类

 @return 类
 */
+ (MySelfIAPObject *)mySelfShareIAP;

/**
 苹果内购充值
 
 @param productId 商品Id
 @param sucessBlock 成功回调  返回成功的购买凭证
 @param failureBlock 失败回调
 */

- (void)mySelfIAPWithProductId:(NSString *)productId withPaySucessBlock:(void (^)(NSString *receiptdataStr))sucessBlock failure:(void (^)(NSString *errorDesc))failureBlock;

/**
 判断用户是否购买过
 @param productId 苹果后台设置的商品id
 @return YES 表示购买过  NO 表示未购买过
 */
- (bool)havePayWithProductId:(NSString *)productId;
//恢复购买
- (void)restorePurchaseWithcompleteHandle;
//分享图片
- (void)openToShareWithImgaeName;
//分享视频
- (void)openToShareWithVideo;
@end

#endif

