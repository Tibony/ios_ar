// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//add start
#include <string>
using namespace std;
#if PLATFORM_IOS
#import <UIKit/UIKit.h>
#import <ReplayKit/ReplayKit.h>
#import <AVKit/AVKit.h>


#import "IOS/IOSAppDelegate.h"
#import "IOS/IOSView.h"
//#import "SlateOpenGLESView.h"
//#import "IOS/SlateOpenGLESView.h"
@class SlateOpenGLESViewController;
//#include "IOS/SlateOpenGLESView.h"
@interface ScreenshotEncoder : UIViewController <UINavigationControllerDelegate>
@end


@interface ShareItem : NSObject

-(instancetype)initWithData:(UIImage *)img andFile:(NSURL *)file;

@property (nonatomic, strong) UIImage *img;
@property (nonatomic, strong) NSURL *path;

@end

@interface TViewController : UIViewController{
    //ScreenRecordTibony *ggg;
    int mycount;
    bool shotModel;

}
@property (nonatomic, strong)AVAssetWriterInput *videoInput;
@property (nonatomic, strong)AVAssetWriterInput *audioInput;
@property (nonatomic, strong)AVAssetWriterInput *micInput;
@property (nonatomic, strong)AVAssetWriter *assetWriter;
@property (nonatomic, strong)NSURL *filePath;
@property (nonatomic, strong)NSString *filePathString;
@property (nonatomic, strong)NSString *filename;

//UIButton


@end
@interface TVideoPlay : UIViewController{
    //bool shotModel;
}
@property(strong,nonatomic)AVPlayer *VideoPlayer;
@property(strong,nonatomic)AVPlayerItem *VideoItem;
@property(strong,nonatomic)AVPlayerLayer *PlayerLayer;
@property(strong,nonatomic)UIButton* Button_Play;
@property (nonatomic, strong)NSString *GetfilePathString;
@property (nonatomic, strong)NSString *Getfilename;
@property (nonatomic, strong)UIButton* Button_pause;
@property bool shotModel;
@end


#endif

//add end


#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Runtime/ImageWrapper/Public/IImageWrapper.h"
#include "Runtime/Engine/Public/UnrealClient.h"
#include "Runtime/Core/Public/Misc/Paths.h"
#include <EngineGlobals.h>
#include "Runtime/Core/Public/GenericPlatform/GenericPlatformFile.h"
#include "Runtime/Core/Public/HAL/PlatformFilemanager.h"
#include <Runtime/Engine/Classes/Engine/Engine.h>
#include "Runtime/Core/Public/HAL/FileManager.h"
#include "Runtime/PakFile/Public/IPlatformFilePak.h"
#include "Runtime/Engine/Classes/Engine/StreamableManager.h"
#include "Runtime/CoreUObject/Public/Misc/PackageName.h"
#include "Runtime/Core/Public/Modules/ModuleManager.h"



#include "Runtime/ImageWrapper/Public/IImageWrapperModule.h"
#include "Runtime/Core/Public/Misc/FileHelper.h"
#include "Runtime/Engine/Classes/Engine/Texture2D.h"
#include "Runtime/Engine/Classes/Engine/Texture2DDynamic.h"
//#include "AppleARKitBlueprintLibrary.h"
#include "MyActor.generated.h"


UENUM(BlueprintType)
enum class EJoyImageFormats : uint8
{
	JPG		UMETA(DisplayName = "JPG        "),
	PNG		UMETA(DisplayName = "PNG        "),
	BMP		UMETA(DisplayName = "BMP        "),
	ICO		UMETA(DisplayName = "ICO        "),
	EXR		UMETA(DisplayName = "EXR        "),
	ICNS	UMETA(DisplayName = "ICNS        ")
};

UCLASS()
class GHJ_API AMyActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AMyActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
    //add start
    UFUNCTION(BlueprintCallable, Category = "Screenshooter")
    bool IsScreenshotDone(FString savePath);
    UFUNCTION(BlueprintCallable, Category = "Screenshooter")
    bool RequestNewScreenshot(FString IFString);    
    UFUNCTION(BlueprintCallable, Category = "Tibonys")
    UTexture2DDynamic* Victory_LoadTexture2D_FromFile(const FString& FullFilePath, EJoyImageFormats ImageFormat, bool& IsValid, int32& Width, int32& Height);
    UFUNCTION(BlueprintCallable, Category = "Screenshooter")
    void SaveNativeScreenshot(FString IFString);
    string FStringCasetosting(FString IFString);
    UFUNCTION(BlueprintCallable, Category = "Base")
    void Base(FString &ProjectPersistentDownloadDir, FString &ProjectContentDir);
    UFUNCTION(BlueprintCallable, Category = "Base")
    void GetAllPaths(TArray<FString>& Files, const FString & FilePath, const FString& Extension);
    UFUNCTION(BlueprintCallable, Category = "Base")
    UObject* LoadPakFile(TArray<FString> &FileList, FString OUTLOG, FString MPoint, FString GetObjectPathFile);
    UFUNCTION(BlueprintCallable, Category = "Base")
    void TCleanFile(FString Path);
    UFUNCTION(BlueprintCallable, Category = "Base")
    bool IsImagePathFromB();
    UFUNCTION(BlueprintCallable, Category = "Base")
    bool IsImagePathFromA();
    UFUNCTION(BlueprintCallable, Category = "Base")
    FString GetTestPathUE();
    UFUNCTION(BlueprintCallable, Category = "Base")
    FString GetLastShotPath();
    UFUNCTION(BlueprintCallable, Category = "Base")
    void ShareImage();
    UFUNCTION(BlueprintCallable, Category = "Weixin")
    bool RegWeixin();
    UFUNCTION(BlueprintCallable, Category = "Weixin")
    void WeixinShareText();
    UFUNCTION(BlueprintCallable, Category = "Weixin")
    bool WeixinShareImage(FString InImagename);
    UFUNCTION(BlueprintCallable, Category = "Weixin")
    bool WeixinShareImagePengyou(FString InImagename);
    UFUNCTION(BlueprintCallable, Category = "Weixin")
    bool WeixinShareImageShouchang(FString InImagename);
    UFUNCTION(BlueprintCallable, Category = "Weixin")
    bool WeixinShareVideo(FString InVideoname);
    UFUNCTION(BlueprintCallable, Category = "Weixin")
    bool WeixinShareVideoPengyou(FString InVideoname);
    UFUNCTION(BlueprintCallable, Category = "Weixin")
    bool WeixinShareVideoShouchang(FString InVideoname);
    UFUNCTION(BlueprintCallable, Category = "Weixin")
    bool isWeixinCanOpen();
    UFUNCTION(BlueprintCallable, Category = "Record")
    bool TRecordStart();
    UFUNCTION(BlueprintCallable, Category = "Record")
    bool TRecordEnd();

    //初始化录屏
    UFUNCTION(BlueprintCallable, Category = "Record")
    void InitCapture();
    
    //准备录屏
    UFUNCTION(BlueprintCallable, Category = "Record")
    void ReadyCapture(FString Videoname);
    
    //开始录屏
    UFUNCTION(BlueprintCallable, Category = "Record")
    void StartCapture();
    
    //停止录屏
    UFUNCTION(BlueprintCallable, Category = "Record")
    void StopCapture();
    
    //保存视频到相册
    UFUNCTION(BlueprintCallable, Category = "Record")
    void SaveVideoToPhotoFunc(FString InVideoname);
    
    //判断是否为ipad
    UFUNCTION(BlueprintCallable, Category = "Record")
    bool isIpad();
    
#if PLATFORM_IOS
    TViewController* TVC;
#endif
};

class ItemTableVisitor : public IPlatformFile::FDirectoryVisitor
{
	virtual bool Visit(const TCHAR * FilenameORDirectroy, bool bIsDirectroy)override
	{
		if (!bIsDirectroy)
		{
			FString filepath = FString(FilenameORDirectroy);

		}
		return true;
	}
};
