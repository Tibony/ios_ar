// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include <string>//转换字符串用到
using namespace std;//转换字符串用到
#include "CoreMinimal.h"

#include "GameFramework/Actor.h"

#include "Runtime/ImageWrapper/Public/IImageWrapper.h"
#include "Runtime/Engine/Public/UnrealClient.h"
#include "Runtime/Core/Public/Misc/Paths.h"
#include <EngineGlobals.h>
#include "Runtime/Core/Public/GenericPlatform/GenericPlatformFile.h"
#include "Runtime/Core/Public/HAL/PlatformFilemanager.h"
#include <Runtime/Engine/Classes/Engine/Engine.h>
#include "Runtime/Core/Public/HAL/FileManager.h"
#include "Runtime/PakFile/Public/IPlatformFilePak.h"
#include "Runtime/Engine/Classes/Engine/StreamableManager.h"
#include "Runtime/CoreUObject/Public/Misc/PackageName.h"
#include "Runtime/Core/Public/Modules/ModuleManager.h"



#include "Runtime/ImageWrapper/Public/IImageWrapperModule.h"
#include "Runtime/Core/Public/Misc/FileHelper.h"
#include "Runtime/Engine/Classes/Engine/Texture2D.h"
#include "Runtime/Engine/Classes/Engine/Texture2DDynamic.h"
#include "GameFramework/GameModeBase.h"
#include "MyProjectGameModeBase.generated.h"

#if PLATFORM_IOS
#include "MyReplaykit.h"//引进录屏用到的类
#include "MySelfIAPObject.h"//引进内购用到的类
#endif


DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnIAPResultDelegate, bool, bSuccess, FString, ResultDes);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FHuiFuPayResultDelegate, bool, bSuccess, FString, ResultStr);
/**
 * 
 */
UCLASS()
class GHJ_API AMyProjectGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
public:
    //转换字符串
    string FStringCasetosting(FString IFString);
    //购买
	UPROPERTY(BlueprintAssignable, Category = "IOSBlueprint")
	FOnIAPResultDelegate OnIAPResult;
    
	UFUNCTION(BlueprintCallable, Category = "IOSBlueprint")
	void IAPWithProductId(FString ProductId);
	//是否购买过
    UFUNCTION(BlueprintCallable, Category = "IOSBlueprint")
    bool IsPay(FString ProductId);
    
    UPROPERTY(BlueprintAssignable, Category = "IOSBlueprint")
    FHuiFuPayResultDelegate HuiFuPayResult;
    //恢复购买
    UFUNCTION(BlueprintCallable, Category = "IOSBlueprint")
    void HuiFuPay();
    //图片分享
    UFUNCTION(BlueprintCallable, Category = "IOSBlueprint")
    void ShareImage();
    //视频分享
    UFUNCTION(BlueprintCallable, Category = "IOSBlueprint")
    void ShareVideo();
    //录制视频
    UFUNCTION(BlueprintCallable, Category = "IOSBlueprint")
    void RecordVideo();
    
    //初始化录屏
    UFUNCTION(BlueprintCallable, Category = "IOSBlueprint")
    void InitRecord();
    //准备录制视频
    UFUNCTION(BlueprintCallable, Category = "IOSBlueprint")
    void ReadyRecord(FString VideoName);
    //开始录制视频
    UFUNCTION(BlueprintCallable, Category = "IOSBlueprint")
    void StartRecord();
    //结束录制视频
    UFUNCTION(BlueprintCallable, Category = "IOSBlueprint")
    void StopRecord();
#if PLATFORM_IOS
    TViewController* MyVideo;//定义录屏类的指针
#endif
};
