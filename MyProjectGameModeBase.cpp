// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProjectGameModeBase.h"
//购买
void AMyProjectGameModeBase::IAPWithProductId(FString ProductId)
{
#if PLATFORM_IOS	
	NSString *str = [NSString stringWithUTF8String : TCHAR_TO_UTF8(*ProductId)];
	void(^IAPSuccess)(NSString *receiptdataStr) = ^ (NSString *receiptdataStr) {

		if (OnIAPResult.IsBound())
		{
			OnIAPResult.Broadcast(true, [receiptdataStr UTF8String]);
		}
	};

	void(^IAPFailure)(NSString *errorDesc) = ^ (NSString *receiptdataStr) {

		if (OnIAPResult.IsBound())
		{
			OnIAPResult.Broadcast(false, [receiptdataStr UTF8String]);
		}
	};

	[[MySelfIAPObject mySelfShareIAP]mySelfIAPWithProductId:str withPaySucessBlock:IAPSuccess failure:IAPFailure];
#endif
}
//是否购买过
bool AMyProjectGameModeBase::IsPay(FString ProductId)
{
    bool result = 0;
#if PLATFORM_IOS
      NSString *str = [NSString stringWithUTF8String : TCHAR_TO_UTF8(*ProductId)];
      result = [[MySelfIAPObject mySelfShareIAP] havePayWithProductId:str];
#endif
    return result;
}
//恢复购买
void AMyProjectGameModeBase::HuiFuPay()
{
#if PLATFORM_IOS    
   [[MySelfIAPObject mySelfShareIAP]restorePurchaseWithcompleteHandle];
#endif
}
//图片分享
void AMyProjectGameModeBase::ShareImage()
{
#if PLATFORM_IOS
    [[MySelfIAPObject mySelfShareIAP]openToShareWithImgaeName];
#endif
}
//视频分享
void AMyProjectGameModeBase::ShareVideo()
{
#if PLATFORM_IOS
    [[MySelfIAPObject mySelfShareIAP]openToShareWithVideo];
#endif
}
//录制视频
void AMyProjectGameModeBase::RecordVideo()
{
#if PLATFORM_IOS
    [[MySelfIAPObject mySelfShareIAP]openToRecordWithVideo];
#endif
}

//初始化录屏
void AMyProjectGameModeBase::InitRecord()
{
#if PLATFORM_IOS
    MyVideo= [[TViewController alloc] init];//初始化录屏类
#endif
}

//转换字符串
string AMyProjectGameModeBase::FStringCasetosting(FString IFString)
{
    std::string HostString(TCHAR_TO_UTF8(*IFString));
    return HostString;
}
//准备录制视频
void AMyProjectGameModeBase::ReadyRecord(FString VideoName)
{    
#if PLATFORM_IOS    
    string CasetoString = FStringCasetosting(VideoName);//接收进来的视频名称参数
    NSString *astring = [NSString stringWithCString : CasetoString.c_str() encoding : [NSString defaultCStringEncoding]];
    [MyVideo StartReadyReplayKitCapture:astring];//调用准备录屏函数
#endif
}
//开始录制视频
void AMyProjectGameModeBase::StartRecord()
{
#if PLATFORM_IOS
    [MyVideo StartReplayKitCapture];//调用开始录屏函数
#endif
}
//结束录制视频
void AMyProjectGameModeBase::StopRecord()
{
#if PLATFORM_IOS
    [MyVideo StopReplaykitCapture];//调用结束录屏函数
#endif
}
