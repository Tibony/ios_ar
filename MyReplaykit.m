//
#if PLATFORM_IOS
#import "MyReplaykit.h"


@interface TViewController ()<RPPreviewViewControllerDelegate,RPScreenRecorderDelegate>
{

}
@end

@implementation TViewController

//

- (void)viewDidLoad {
    [super viewDidLoad];
    CGRect frame = CGRectMake(90, 1066, 600, 60);
}


- (void)previewControllerDidFinish:(RPPreviewViewController *)previewController {
    [previewController dismissViewControllerAnimated:YES completion:nil];
    
}


//录屏准备
-(void)StartReadyReplayKitCapture:(NSString *)VideoName
{
    CGFloat VideoWidth=  UIScreen.mainScreen.bounds.size.width;
    CGFloat VideoHeight = UIScreen.mainScreen.bounds.size.height;
    
    NSDictionary *Infos;
    
    
    //按设备分辨率录的视频
    CGFloat Resolution_W = [UIScreen mainScreen].scale * WIDTH*0.5;
    CGFloat Resolution_H = [UIScreen mainScreen].scale * HIGHT*0.5;
    Infos =@{@"AVVideoCodecKey":AVVideoCodecTypeH264,@"AVVideoWidthKey":@(Resolution_W),@"AVVideoHeightKey":@(Resolution_H)};
   
    
    _videoInput=[[AVAssetWriterInput alloc]initWithMediaType:AVMediaTypeVideo outputSettings:Infos];
    NSError *Error;
    NSString *VideoPath=[NSString stringWithFormat:@"%@/Documents/GHJ/Saved/%@.mp4",NSHomeDirectory(),VideoName];
    self.filename = VideoName;
    self.filePathString = VideoPath;
    self.filePath = [NSURL fileURLWithPath:VideoPath];
    _assetWriter = [[AVAssetWriter alloc]initWithURL:self.filePath fileType:AVFileTypeMPEG4 error:&Error];
    _videoInput.expectsMediaDataInRealTime = true;
    [_assetWriter addInput:_videoInput];
    

    [[RPScreenRecorder sharedRecorder] setMicrophoneEnabled:true];
        
 
    AudioChannelLayout acl;
    bzero( &acl, sizeof(acl));
    acl.mChannelLayoutTag = kAudioChannelLayoutTag_Mono;
    NSDictionary *audioOutputSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                         [NSNumber numberWithInteger:kAudioFormatMPEG4AAC], AVFormatIDKey,
                                         [ NSNumber numberWithFloat: 44100.0 ], AVSampleRateKey,
                                         [NSNumber numberWithInt:64000], AVEncoderBitRatePerChannelKey,
                                         [ NSData dataWithBytes: &acl length: sizeof( acl ) ], AVChannelLayoutKey,
                                         nil];
    _micInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeAudio outputSettings:audioOutputSettings];
    _micInput.expectsMediaDataInRealTime = true;
    if( [_assetWriter canAddInput:_micInput])
    {
        [_assetWriter addInput:_micInput];
    }
   

}


//录屏
-(void)StartReplayKitCapture:(NSString *)VideoName
{  
    [[RPScreenRecorder sharedRecorder]startCaptureWithHandler:^(CMSampleBufferRef  _Nonnull sampleBuffer, RPSampleBufferType bufferType, NSError * _Nullable error) {
        
        if(CMSampleBufferDataIsReady(sampleBuffer))
        {

            if(bufferType == RPSampleBufferTypeVideo)
            {

                if(_assetWriter.status ==AVAssetWriterStatusUnknown)
                {
                    [_assetWriter startWriting];
                    [_assetWriter startSessionAtSourceTime:CMSampleBufferGetPresentationTimeStamp(sampleBuffer)];   
            
                 }
                 
                if([_videoInput isReadyForMoreMediaData])
                {
                        [_videoInput appendSampleBuffer:sampleBuffer];
                }
            }
            if(bufferType == RPSampleBufferTypeAudioMic)
            {

                if([_micInput isReadyForMoreMediaData])
                {
                    [_micInput appendSampleBuffer:sampleBuffer];
                }
            }
        }
        
        
    } completionHandler:^(NSError * _Nullable error) {
        
    }
     
     
     ];
}







//停止录屏
-(void)StopReplaykitCapture
{
    [_videoInput markAsFinished];
    [_micInput markAsFinished];
    [[RPScreenRecorder sharedRecorder] stopCaptureWithHandler:^(NSError * _Nullable error) {
        [_assetWriter finishWritingWithCompletionHandler:^{
            TVideoPlay *TVPlayer = [[TVideoPlay alloc] init];
            TVPlayer.view.backgroundColor = [UIColor whiteColor];
            NSString * path = [self.filePath absoluteString];
            [TVPlayer PlayUrlFromString:self.filePath setPathString:self.filePathString setfileName:self.filename];
            [[IOSAppDelegate GetDelegate].IOSController presentViewController : TVPlayer animated : YES completion : nil];
        }];
    }];
}

-(void)SavePhotoDone
{
    
}

-(void)SaveVideoToPhoto:(NSString *)VideoName
{
    NSString *VideoPath=[NSString stringWithFormat:@"%@/Documents/GHJ/Saved/%@.mp4",NSHomeDirectory(),VideoName];
    NSURL *FilePath = [NSURL fileURLWithPath:VideoPath];
    
    UISaveVideoAtPathToSavedPhotosAlbum(VideoPath, nil, nil, nil);
}

@end
#endif