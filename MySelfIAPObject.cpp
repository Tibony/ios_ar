//  TMySelfIAPObject.m
//  MySelf
//
//  Created by MySelf on 2018/7/13.
//  Copyright © 2018年 MySelf. All rights reserved.
//  苹果内购单例类
#if PLATFORM_IOS
#import "MySelfIAPObject.h"
#import <StoreKit/StoreKit.h>
#import <StoreKit/SKPaymentTransaction.h>
#import <ReplayKit/ReplayKit.h>

static MySelfIAPObject *applePay = nil;
@interface MySelfIAPObject()<SKProductsRequestDelegate,SKPaymentTransactionObserver,UIPopoverPresentationControllerDelegate,RPPreviewViewControllerDelegate>
{
    NSString *flag;
    SKProductsRequest *req;
}
@property(nonatomic, copy)void (^IAPSucessBlock)(NSString *);//成功回调block
@property(nonatomic, copy)void (^IAPFailureBlock)(NSString *);//失败回调block
@property(nonatomic, copy) NSString *productId;//商品id

@property (nonatomic, strong) NSTimer *timer;//记录录制时间

@property(nonatomic,assign)NSInteger index;
@end
@implementation MySelfIAPObject

+ (MySelfIAPObject *)mySelfShareIAP{
    if (applePay == nil)
    {
        applePay = [[MySelfIAPObject alloc] init];
    }
    return applePay;
}


 //分享图片
- (void)openToShareWithImgaeName {
    NSString *fullPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/GHJ/Saved/Screenshots/IOS/ScreenShot00000.png"];
    UIImage *shareImage = [[UIImage alloc] initWithContentsOfFile:fullPath];
    NSData *data = UIImagePNGRepresentation(shareImage);
    NSArray *activityItemsArray = @[data];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItemsArray applicationActivities:nil];
    activityVC.modalInPopover = NO;
    UIActivityViewControllerCompletionWithItemsHandler itemsBlock = ^(UIActivityType __nullable activityType, BOOL completed, NSArray * __nullable returnedItems, NSError * __nullable activityError){
        if (completed == YES) {
            NSLog(@"completed");
        }else{
            NSLog(@"cancel");
        }
    };
    activityVC.completionWithItemsHandler = itemsBlock;
    UIViewController *rootVC = [UIApplication sharedApplication].delegate.window.rootViewController;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        activityVC.popoverPresentationController.sourceView = rootVC.view;
        activityVC.popoverPresentationController.sourceRect = CGRectMake(rootVC.view.center.x-rootVC.view.frame.size.width * 300/750 * 1/2, rootVC.view.center.y-rootVC.view.frame.size.height * 300/750 * 1/2, rootVC.view.frame.size.width * 300/750, rootVC.view.frame.size.height * 300/750);//尖尖显示的时候是加号，不显示的时候是减号，rootVC.view.center.y+rootVC.view.frame.size.height * 300/750 * 1/2,
        activityVC.popoverPresentationController.delegate = self;
        [activityVC.popoverPresentationController setPermittedArrowDirections:0];//尖尖不显示
        //[activityVC.popoverPresentationController setPermittedArrowDirections: UIPopoverArrowDirectionUp];//尖尖超上
        [rootVC presentViewController:activityVC animated:YES completion:nil];
    } else {
        [rootVC presentViewController:activityVC animated:YES completion:nil];
    }
}
 //分享视频
- (void)openToShareWithVideo {
    NSString *fullPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/GHJ/Saved/video.mp4"];
    NSArray *activityItemsArray = @[[NSURL fileURLWithPath:fullPath]];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItemsArray applicationActivities:nil];
    activityVC.modalInPopover = NO;
    UIActivityViewControllerCompletionWithItemsHandler itemsBlock = ^(UIActivityType __nullable activityType, BOOL completed, NSArray * __nullable returnedItems, NSError * __nullable activityError){
        if (completed == YES) {
            NSLog(@"completed");
        }else{
            NSLog(@"cancel");
        }
    };
    activityVC.completionWithItemsHandler = itemsBlock;
    UIViewController *rootVC = [UIApplication sharedApplication].delegate.window.rootViewController;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        activityVC.popoverPresentationController.sourceView = rootVC.view;
        activityVC.popoverPresentationController.sourceRect = CGRectMake(rootVC.view.center.x-rootVC.view.frame.size.width * 300/750 * 1/2, rootVC.view.center.y-rootVC.view.frame.size.height * 300/750 * 1/2, rootVC.view.frame.size.width * 300/750, rootVC.view.frame.size.height * 300/750);//尖尖显示的时候是加号，不显示的时候是减号，rootVC.view.center.y+rootVC.view.frame.size.height * 300/750 * 1/2,
        activityVC.popoverPresentationController.delegate = self;
        [activityVC.popoverPresentationController setPermittedArrowDirections:0];//尖尖不显示
        //[activityVC.popoverPresentationController setPermittedArrowDirections: UIPopoverArrowDirectionUp];//尖尖超上
        [rootVC presentViewController:activityVC animated:YES completion:nil];
    } else {
        [rootVC presentViewController:activityVC animated:YES completion:nil];
    }
}

//录制视频
- (void)openToRecordWithVideo {

    
    if ([RPScreenRecorder sharedRecorder].available) {
        NSLog(@"OK");
        [self timerFired];
        //如果支持，就使用下面的方法可以启动录制回放
        
        if (@available(iOS 10.0, *)) {
            [[RPScreenRecorder sharedRecorder] startRecordingWithHandler:^(NSError * _Nullable error) {}];
        }
      
    } else {
        NSLog(@"录制回放功能不可用");
    }
    
}
//查看视频
- (void)openToviewWithVideo{
    
    [[RPScreenRecorder sharedRecorder] stopRecordingWithHandler:^(RPPreviewViewController * _Nullable previewViewController, NSError * _Nullable error) {
     if (error) {
     NSLog(@"%@", error);
     //处理发生的错误，如磁盘空间不足而停止等
     }
     if (previewViewController) {
     //设置预览页面到代理
     UIViewController *rootVC = [UIApplication sharedApplication].delegate.window.rootViewController;

     previewViewController.previewControllerDelegate = self;
     [rootVC presentViewController:previewViewController animated:YES completion:nil];
     }
     }];
}
//回放预览界面的代理方法
- (void)previewControllerDidFinish:(RPPreviewViewController *)previewController {
    //用户操作完成后，返回之前的界面
    [previewController dismissViewControllerAnimated:YES completion:nil];
    
}

/**
 *  开启定时器
 */
- (void)timerFired
{
    self.index = 0;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(timerRecord) userInfo:nil repeats:YES];
}
/**
 *  时间
 */
- (void)timerRecord{
    
    self.index ++;
    
    if(self.index == 10){
        [self openToviewWithVideo];
    }
    
}
/**
 *  停止定时器
 */
- (void)timerStop
{
    if ([self.timer isValid])
    {
        [self.timer invalidate];
        self.timer = nil;
    }
}

- (BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    return YES;
}


/**
 苹果内购
 
 @param productId 商品Id
 @param sucessBlock 成功回调
 @param failureBlock 失败回调
 */

- (void)mySelfIAPWithProductId:(NSString *)productId withPaySucessBlock:(void (^)(NSString *receiptdataStr))sucessBlock failure:(void (^)(NSString *errorDesc))failureBlock
{
    flag = productId;
    if (![SKPaymentQueue canMakePayments])
    {
        if (failureBlock) {
            failureBlock(@"用户未开通购买");
        }
        return;
    }
  
    self.IAPSucessBlock = ^(NSString *receiptdataStr){
      
        if (sucessBlock) {
            sucessBlock(receiptdataStr);
        }
    };
    
    self.IAPFailureBlock = ^(NSString *errDesc) {
        if (failureBlock) {
            failureBlock(errDesc);
        }
    };
    
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    //[[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
    NSSet * set = [NSSet setWithArray:@[productId]];
    NSLog(@"[内购的产品:@[productId]]==%@",productId);
    self.productId=productId;
    req= [[SKProductsRequest alloc] initWithProductIdentifiers:set];
    req.delegate = self;
    [req start];
}

- (void)dealloc {
    [super dealloc];
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
}

//恢复购买
- (void)restorePurchaseWithcompleteHandle{
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}
//恢复购买结束回调
- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue NS_AVAILABLE_IOS(3_0){
    UIViewController *rootvc = [UIApplication sharedApplication].delegate.window.rootViewController;
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"恢复购买成功" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"ok" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:action];
    [rootvc presentViewController:alert animated:YES completion:nil];
    NSLog(@"restoreSuccess");
}

//恢复购买失败
- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error NS_AVAILABLE_IOS(3_0){
    UIViewController *rootvc = [UIApplication sharedApplication].delegate.window.rootViewController;
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"恢复购买失败" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"ok" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:action];
    [rootvc presentViewController:alert animated:YES completion:nil];
    NSLog(@"failure");
}

- (void)restoreTransaction:(SKPaymentTransaction *)transaction operationId:(NSString *)operationId {
    
    [self verifyPurchaseWithPaymentTransaction:transaction isTestServer:NO operationId:operationId];
}

// 完成交易
- (void)completeTransaction:(SKPaymentTransaction *)transaction operationId:(NSString *)operationId {
    
    [self verifyPurchaseWithPaymentTransaction:transaction isTestServer:NO operationId:operationId];
}

#pragma mark - 请求回调
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    NSArray *myProduct = response.products;
    
    if (myProduct.count == 0) {
        NSLog(@"%@标示错误",flag);
        req.delegate = nil;
        req = nil;
        return;
    }
    SKMutablePayment *pakment = [SKMutablePayment paymentWithProduct:myProduct[0]];
    
    [[SKPaymentQueue defaultQueue] addPayment:pakment];
}
#pragma mark - IAP返回结果
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions {
    
    for (SKPaymentTransaction *transaction in transactions)
    {
//        transaction.
        NSInteger state = transaction.transactionState;
        if (state == SKPaymentTransactionStatePurchased)
        {
            NSLog(@" -- IAP完成");
            //IAP完成
            [self TTW_completeTransaction:transaction];
            req.delegate = nil;
            req = nil;
        }
        else if (state == SKPaymentTransactionStateFailed)
        {
             NSLog(@" -- IAP失败");
            //IAP失败
            [self TTW_failedTransaction:transaction];
            req.delegate = nil;
            req = nil;
        }
        else if (state == SKPaymentTransactionStateRestored)
        {
            //恢复购买
            [self TTWRestoreTransaction:transaction];
            req.delegate = nil;
            req = nil;
        }
        else if (state == SKPaymentTransactionStatePurchasing)
        {
            NSLog(@" -- 商品添加进列表");
        }
        else
        {
             NSLog(@" -- 最终状态未确定 ");
        }
    }
    NSLog(@" -- transactions %@",transactions);
}
#pragma mark - 成功调用方法
- (void)TTW_completeTransaction:(SKPaymentTransaction *)transaction
{
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
    NSString * productIdentifier = transaction.payment.productIdentifier;
    NSURL *receiptUrl = [[NSBundle mainBundle] appStoreReceiptURL];
    NSData *receiptData = [NSData dataWithContentsOfURL:receiptUrl];
    
    //${replaceCode}
    
    NSString *TTW_jsonObjectStringTTW = [receiptData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    if ([productIdentifier length] > 0)
    {
        
        NSMutableArray *mArr=[NSMutableArray array];
        NSArray *arr=[[NSUserDefaults standardUserDefaults] objectForKey:@"myGame"];
        if (arr.count>0) {
            [mArr addObjectsFromArray:arr];
        }
        BOOL have=NO;
        for (NSString *str in mArr) {
            if ([str isEqualToString:self.productId]) {
                have=YES;
            }
        }
        if (have==NO) {
            [mArr addObject:self.productId];
        }
        
        
        [[NSUserDefaults standardUserDefaults] setObject:mArr forKey:@"myGame"];
        if (self.IAPSucessBlock) {
            self.IAPSucessBlock(TTW_jsonObjectStringTTW);
        }
        
    }
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    
}
#pragma mark - 失败调用方法
- (void)TTW_failedTransaction:(SKPaymentTransaction *)transaction
{
    
    NSString *errorStr = @"";
    if(transaction.error.code == SKErrorUnknown)
    {
        NSLog(@"购买失败");
      errorStr=[transaction.error localizedDescription];
    }
    else if (transaction.error.code == SKErrorClientInvalid)
    {
       errorStr=@"客户端不允许发出请求，请查看设备网络";
    }
    else if (transaction.error.code == SKErrorPaymentInvalid)
    {
        errorStr=@"购买标识符是无效的 ，请联系客服";
    }
    else if (transaction.error.code == SKErrorPaymentNotAllowed)
    {
        errorStr=@"此设备不允许支付，请联系客服";
    }
    else if (transaction.error.code == SKErrorStoreProductNotAvailable)
    {
        errorStr=@"目前的产品不可用 购买失败";
    }
    else if (@available(iOS 9.3, *)) {
        if (transaction.error.code == SKErrorCloudServicePermissionDenied)
        {
            errorStr=@"用户已不允许访问云服务信息，请查看设备设置";
        }
        else if (transaction.error.code == SKErrorCloudServiceNetworkConnectionFailed)
        {
           errorStr=@"此设备不能连接网络，请查看设备网络";
        }
        else if (transaction.error.code == SKErrorPaymentCancelled)
        {
            errorStr=@"交易取消";
        }
        else
        {
           errorStr=@"购买失败，请重新购买";
        }
    } else {
        // Fallback on earlier versions
        errorStr=@"购买失败，请重新购买";
    }
    
    if (self.IAPFailureBlock) {
        self.IAPFailureBlock(errorStr);
    }
    
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}



- (void)TTWRestoreTransaction:(SKPaymentTransaction *)transaction
{
    if (self.IAPFailureBlock) {
        self.IAPFailureBlock(@"你已经购买过了");
    }
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}
/**
 判断用户是否购买过
 @param productId 苹果后台设置的商品id
 @return YES 表示购买过  NO 表示未购买过
 */
-(bool)havePayWithProductId:(NSString *)productId
{
    NSArray *arr=[[NSUserDefaults standardUserDefaults] objectForKey:@"myGame"];
    bool have=false;
    
    for (NSString *str in arr) {
        if ([str isEqualToString:productId]) {
            have=true;
        }
    }
    return have;    
}

- (void)verifyPurchaseWithPaymentTransaction:(SKPaymentTransaction *)transaction isTestServer:(BOOL)flag operationId:(NSString *)operationId {

    //交易验证
    NSURL *recepitURL = [[NSBundle mainBundle] appStoreReceiptURL];
    NSData *receipt = [NSData dataWithContentsOfURL:recepitURL];
    if (!receipt) {
        // 交易凭证为空验证失败
        
        return;
    }
    // 购买成功将交易凭证发送给服务端进行再次校验
    NSError *error;
    NSDictionary *requestContents = @{
        @"receipt-data": [receipt base64EncodedStringWithOptions:0]
    };
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:requestContents
                                                          options:0
                                                            error:&error];
    
    if (!requestData) { // 交易凭证为空验证失败
        return;
    }
    
    //In the test environment, use https://sandbox.itunes.apple.com/verifyReceipt
    //In the real environment, use https://buy.itunes.apple.com/verifyReceipt
    
    NSString *serverString = @"https://buy.itunes.apple.com/verifyReceipt";
    if (flag) {
        serverString = @"https://sandbox.itunes.apple.com/verifyReceipt";
    }
    NSMutableURLRequest *storeRequest = [NSMutableURLRequest requestWithURL:[[NSURL alloc] initWithString:serverString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0f];
    [storeRequest setHTTPMethod:@"POST"];
    [storeRequest setHTTPBody:requestData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionTask *dataTask = [session dataTaskWithRequest:storeRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (!error) {
            NSError *error;
            NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
            if (!jsonResponse) {
                // 苹果服务器校验数据返回为空校验失败
                NSLog(@"验证失败");
            }
            
            // 先验证正式服务器,如果正式服务器返回21007再去苹果测试服务器验证,沙盒测试环境苹果用的是测试服务器
            NSString *status = [NSString stringWithFormat:@"%@", jsonResponse[@"status"]];
            if (status && [status isEqualToString:@"21007"]) {
                [self verifyPurchaseWithPaymentTransaction:transaction isTestServer:YES operationId:operationId];
            } else if (status && [status isEqualToString:@"0"]) {
                //订单校验成功
                
            }
        } else {
            // 无法连接服务器,购买校验失败
            NSLog(@"链接失败");
        }
    }];
}
    
    // 验证成功与否都注销交易,否则会出现虚假凭证信息一直验证不通过,每次进程序都得输入苹果账号

@end
#endif
