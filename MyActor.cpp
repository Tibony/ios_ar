#include "MyActor.h"
#include "MySelfIAPObject.h"
#if PLATFORM_IOS
#include "IOSAppDelegate.h"
#import <Foundation/Foundation.h>
#import "AlphaLibrary.h"
#import "WXApi.h"
#import "WXApiObject.h"
#import "WechatAuthSDK.h"

@class AlphaLibrary;
#endif

#if PLATFORM_IOS

#include "IOS/IOSApplication.h"
#include "IOSAppDelegate.h"
#include "IOSView.h"
#endif

#if PLATFORM_IOS
#define WIDTH [UIScreen mainScreen].bounds.size.width
#define HIGHT  [UIScreen mainScreen].bounds.size.height
@interface ScreenshotEncoder()
@end
@implementation ScreenshotEncoder
+ (ScreenshotEncoder*)GetDelegate
{
	static ScreenshotEncoder* Singleton = [[ScreenshotEncoder alloc] init];
	return Singleton;
}

//weixin start
+(bool)RegWeixinTOB
{
    return [WXApi registerApp:@"wx7a0895f6bd5ee068"];
}

+(void)ShareTextTOB
{

    NSString *aPath3=@"ok";
    SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
    req.text = aPath3;
    req.bText = YES;
    req.scene = WXSceneSession;
    [WXApi sendReq:req];
}

+(bool)ShareImageTOB:(NSString *)ImageName
{
    bool isSuccee = 0;
    NSString *aPath3=[NSString stringWithFormat:@"%@/Documents/GHJ/Saved/Screenshots/IOS/%@.png",NSHomeDirectory(),ImageName];
    WXMediaMessage *message =[WXMediaMessage message];
    WXImageObject *imageObject = [WXImageObject object];
    imageObject.imageData = [NSData dataWithContentsOfFile:aPath3];
    if(imageObject.imageData != NULL)
    {
        isSuccee = 1;
    }
    message.mediaObject = imageObject;
    
    SendMessageToWXReq* req =[[SendMessageToWXReq alloc] init];
    req.bText = NO;
    req.message = message;
    req.scene = WXSceneSession;
    
    [WXApi sendReq:req];
    return isSuccee;
}

+(bool)ShareImageTOBPengyou:(NSString *)ImageName
{
    bool isSuccee = 0;
    NSString *aPath3=[NSString stringWithFormat:@"%@/Documents/GHJ/Saved/Screenshots/IOS/%@.png",NSHomeDirectory(),ImageName];
    WXMediaMessage *message =[WXMediaMessage message];
    WXImageObject *imageObject = [WXImageObject object];
    imageObject.imageData = [NSData dataWithContentsOfFile:aPath3];
    if(imageObject.imageData != NULL)
    {
        isSuccee = 1;
    }
    message.mediaObject = imageObject;
    
    SendMessageToWXReq* req =[[SendMessageToWXReq alloc] init];
    req.bText = NO;
    req.message = message;
    req.scene = WXSceneTimeline;
    
    [WXApi sendReq:req];
    return isSuccee;
}

+(bool)ShareImageTOBShouchang:(NSString *)ImageName
{
    bool isSuccee = 0;
    NSString *aPath3=[NSString stringWithFormat:@"%@/Documents/GHJ/Saved/Screenshots/IOS/%@.png",NSHomeDirectory(),ImageName];
    WXMediaMessage *message =[WXMediaMessage message];
    WXImageObject *imageObject = [WXImageObject object];
    imageObject.imageData = [NSData dataWithContentsOfFile:aPath3];
    if(imageObject.imageData != NULL)
    {
        isSuccee = 1;
    }
    message.mediaObject = imageObject;
    
    SendMessageToWXReq* req =[[SendMessageToWXReq alloc] init];
    req.bText = NO;
    req.message = message;
    req.scene = WXSceneFavorite;
    
    [WXApi sendReq:req];
    return isSuccee;
}


//Video
+(bool)ShareVideoTOBShouchang:(NSString *)VideoName
{
    bool isSuccee = 0; 
    NSString *aPath3=[NSString stringWithFormat:@"%@/Documents/GHJ/Saved/%@.mp4",NSHomeDirectory(),VideoName];
    WXMediaMessage *message =[WXMediaMessage message];
    WXVideoObject *videoObject =[WXVideoObject object];
    videoObject.videoUrl = aPath3;
    videoObject.videoLowBandUrl =videoObject.videoUrl;
	message.mediaObject = videoObject;
    SendMessageToWXReq* req =[[SendMessageToWXReq alloc] init];
    req.bText = NO;
    req.message = message;
    req.scene = WXSceneFavorite;
    [WXApi sendReq:req];
    return isSuccee;
}

+(bool)ShareVideoTOBPengYou:(NSString *)VideoName
{
    bool isSuccee = 0;
    NSString *aPath3=[NSString stringWithFormat:@"%@/Documents/GHJ/Saved/%@.mp4",NSHomeDirectory(),VideoName];
    WXFileObject *FileObjectVideo = [WXFileObject object];
    FileObjectVideo.fileExtension = @"mp4";
    FileObjectVideo.fileData = [NSData dataWithContentsOfFile:aPath3];
    WXMediaMessage *message =[WXMediaMessage message];
    message.mediaObject = FileObjectVideo;
    SendMessageToWXReq* req =[[SendMessageToWXReq alloc] init];
    req.bText = NO;
    req.message = message;
    req.scene = WXSceneTimeline;
    [WXApi sendReq:req];
    return isSuccee;
}

+(bool)ShareVideoTOBHaoyou:(NSString *)VideoName
{

    bool isSuccee = 0;
    NSString *aPath3=[NSString stringWithFormat:@"%@/Documents/GHJ/Saved/%@.mp4",NSHomeDirectory(),VideoName];
    WXFileObject *FileObjectVideo = [WXFileObject object];
    FileObjectVideo.fileExtension = @"mp4";
    FileObjectVideo.fileData = [NSData dataWithContentsOfFile:aPath3];
    WXMediaMessage *message =[WXMediaMessage message];
    message.mediaObject = FileObjectVideo;
    SendMessageToWXReq* req =[[SendMessageToWXReq alloc] init];
    req.bText = NO;
    req.message = message;
    req.scene = WXSceneSession;
    [WXApi sendReq:req];
    return isSuccee;
}

//weixin end





+(void)addActivityViewController
{

    NSArray *activityItems = @[
                               @"http://img3.duitang.com/uploads/item/201604/24/20160424132044_ZzhuX.jpeg",
                               @"http://v1.qzone.cc/avatar/201408/03/23/44/53de58e5da74c247.jpg%21200x200.jpg",
                               @"http://img4.imgtn.bdimg.com/it/u=1483569741,1992390913&fm=214&gp=0.jpg"];
    NSMutableArray *items = [NSMutableArray array];
    NSString *docPath = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0];
    for (int i = 0; i < activityItems.count; i++) {

        NSString *URL = [activityItems[i] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

        NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:URL]];

        UIImage *imagerang = [UIImage imageWithData:data];

        NSString *imagePath = [docPath stringByAppendingString:[NSString stringWithFormat:@"/ShareWX%d.jpg",i]];

        [UIImageJPEGRepresentation(imagerang, .5) writeToFile:imagePath atomically:YES];

        NSURL *shareobj = [NSURL fileURLWithPath:imagePath];

        ShareItem *item = [[ShareItem alloc] initWithData: imagerang andFile:shareobj];

        [items addObject:item];
    }

    UIActivityViewController *activityVC = [[UIActivityViewController alloc]initWithActivityItems:items applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypePostToFacebook,UIActivityTypePostToTwitter, UIActivityTypePostToWeibo,UIActivityTypeMessage,UIActivityTypeMail,UIActivityTypePrint,UIActivityTypeCopyToPasteboard,UIActivityTypeAssignToContact,UIActivityTypeSaveToCameraRoll,UIActivityTypeAddToReadingList,UIActivityTypePostToFlickr,UIActivityTypePostToVimeo,UIActivityTypePostToTencentWeibo,UIActivityTypeAirDrop,UIActivityTypeOpenInIBooks];
    
    [[ScreenshotEncoder GetDelegate] presentViewController: activityVC animated:YES completion:nil];
}


+(UIImage*)imageWithColor:(UIColor *)color {
	CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f); //øÌ∏ﬂ 1.0÷ª“™”–÷µæÕπª¡À
	UIGraphicsBeginImageContext(rect.size); //‘⁄’‚∏ˆ∑∂Œßƒ⁄ø™∆Ù“ª∂Œ…œœ¬Œƒ
	CGContextRef context = UIGraphicsGetCurrentContext();

	CGContextSetFillColorWithColor(context, [color CGColor]);//‘⁄’‚∂Œ…œœ¬Œƒ÷–ªÒ»°µΩ—’…´UIColor
	CGContextFillRect(context, rect);//”√’‚∏ˆ—’…´ÃÓ≥‰’‚∏ˆ…œœ¬Œƒ

	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();//¥”’‚∂Œ…œœ¬Œƒ÷–ªÒ»°Image Ù–‘,,,Ω· ¯
	UIGraphicsEndImageContext();

	return image;
}




//保存图片到相册
+(void)saveShot:(NSString *)Paths
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex : 0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent : @"image.png"];
	NSString *aPath3=[NSString stringWithFormat:@"%@/Documents/GHJ/Saved/Screenshots/IOS/%@.png",NSHomeDirectory(),Paths];
	UIImage *image = [UIImage imageWithContentsOfFile : aPath3];
	NSLog(@"url=%@",aPath3);
    if(image == NULL)
    {
        NSLog(@"image == null");
    }
	UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
}

+(void)saveScreenshotNative:(NSString *)NativePaths
{
	[[ScreenshotEncoder GetDelegate] performSelectorOnMainThread:@selector(saveShot) withObject:NativePaths waitUntilDone : NO];
}

+(const char*)GetFilePath
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex : 0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent : @"image.png"];
		const char *destDir = [filePath UTF8String];
	return destDir;
}

+(bool)IsImagePathFromA
{
    NSString *aPath3=[NSString stringWithFormat:@"%@/Documents/GHJ/Saved/Screenshots/IOS/%@.png",NSHomeDirectory(),@"ScreenShot00000"];
    UIImage *image = [UIImage imageWithContentsOfFile : aPath3];
    if(image == NULL)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

+(bool)IsImagePathFromB
{
    NSString *aPath3=[NSString stringWithFormat:@"%@/Documents/GHJ/Saved/Screenshots/IOS/%@.png",NSHomeDirectory(),@"ScreenShot00000"];
    UIImage *image=[[UIImage alloc]initWithContentsOfFile:aPath3];
     if(image == NULL)
     {
         return 0;
     }
    else
    {
        return 1;
    }
}

+(const char*)GetTestPath
{
    NSString *aPath3=[NSString stringWithFormat:@"%@/Documents/GHJ/Saved/Screenshots/IOS/%@.png",NSHomeDirectory(),@"ScreenShot00000"];
    const char *destDir = [aPath3 UTF8String];
    return destDir;
}



+(const char*)GetFileList
{

	//Œƒº˛≤Ÿ◊˜∂‘œÛ
	NSFileManager *manager = [NSFileManager defaultManager];
	//Œƒº˛º–¬∑æ∂
	//NSString *home = [@"~" stringByExpandingTildeInPath];//∏˘ƒø¬ºŒƒº˛º–
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *home = [paths objectAtIndex : 0];
	//ƒø¬ºµ¸¥˙∆˜
	NSDirectoryEnumerator *direnum = [manager enumeratorAtPath : home];
	//–¬Ω® ˝◊È£¨¥Ê∑≈∏˜∏ˆŒƒº˛¬∑æ∂
	NSMutableArray *files = [NSMutableArray arrayWithCapacity : 42];
	//±È¿˙ƒø¬ºµ¸¥˙∆˜£¨ªÒ»°∏˜∏ˆŒƒº˛¬∑æ∂
	NSString *filename;
	NSString *Counts = [NSString stringWithFormat : @"FilesCount: %lu",[files count]];
	//myQuickMethod();
	const char *destDir = [Counts UTF8String];
	return destDir;
}


+(bool)showItemInTmall4iOS:(NSString *)itemId
{
	NSURL *url;
	url = [NSURL URLWithString : [itemId stringByAddingPercentEscapesUsingEncoding : NSUTF8StringEncoding]];
	return[[UIApplication sharedApplication] openURL:url];
}

+(void)showItemInTaobao4iOS:(NSString *)itemId
{
	// ππΩ®Ã‘±¶øÕªß∂À–≠“Èµƒ URL
	NSURL *url = [NSURL URLWithString : [NSString stringWithFormat : @"taobao://item.taobao.com/item.htm?id=%@", itemId]];

																			  // ≈–∂œµ±«∞œµÕ≥ «∑Ò”–∞≤◊∞Ã‘±¶øÕªß∂À
		if ([[UIApplication sharedApplication] canOpenURL:url]) {
			// »Áπ˚“—æ≠∞≤◊∞Ã‘±¶øÕªß∂À£¨æÕ π”√øÕªß∂À¥Úø™¡¥Ω”
			[[UIApplication sharedApplication] openURL:url];

		}
		else {
			// ∑Ò‘Ú π”√ Mobile Safari ªÚ’ﬂƒ⁄«∂ WebView ¿¥œ‘ æ
			url = [NSURL URLWithString : [NSString stringWithFormat : @"http://item.taobao.com/item.htm?id=%@", itemId]];
																			 //        [[UIApplication sharedApplication] openURL:url];

																			 // [self tongwanWeb:url];
		}

}



@end

@implementation ShareItem
-(instancetype)initWithData:(UIImage *)img andFile:(NSURL *)file
{
    self = [super init];
    if (self) {
        _img = img;
        _path = file;
    }
    return self;
}

-(instancetype)init
{
    return nil;
}

-(id)activityViewControllerPlaceholderItem:(UIActivityViewController *)activityViewController
{
    return _img;
}

-(id)activityViewController:(UIActivityViewController *)activityViewController itemForActivityType:(NSString *)activityType
{
    return _path;
}

-(NSString*)activityViewController:(UIActivityViewController *)activityViewController subjectForActivityType:(NSString *)activityType
{
    return @"";
}

@end

//
@interface TVideoPlay()<UIPopoverPresentationControllerDelegate>
@end

@implementation TVideoPlay


-(UIImage*)GetFirstFrameImage:(NSURL*)videoURL atTime:(NSTimeInterval)time
{
    UIImage* ReturnImage;
    AVURLAsset *asset = [[AVURLAsset alloc]initWithURL:videoURL options:nil];
    NSCParameterAssert(asset);
    AVAssetImageGenerator* assetImageGenerator =[[AVAssetImageGenerator alloc]initWithAsset:asset];
    assetImageGenerator.appliesPreferredTrackTransform = YES;
    assetImageGenerator.apertureMode = AVAssetImageGeneratorApertureModeEncodedPixels;
    
    CGImageRef thumbnailImageRef = NULL;
    CFTimeInterval thumbnailImageTime = time;
    NSError* thumbnailImageGenerationError = nil;
    
    thumbnailImageRef =[assetImageGenerator copyCGImageAtTime:CMTimeMake(thumbnailImageTime, 25) actualTime:NULL error:&thumbnailImageGenerationError];
    
    ReturnImage = [[UIImage alloc]initWithCGImage:thumbnailImageRef];
    
    
    return ReturnImage;
}


///////////////////////////////////////////////点击事件
//取消
-(void)ClickCancel
    {
        [self.VideoPlayer pause];
        [self dismissViewControllerAnimated:YES completion:nil];
    }

//播放
-(void)ClickPlay
{

    [self.VideoPlayer play];
    [_Button_Play setHidden:true];//隐藏播放按钮
}
//暂停
-(void)ClickPause
{
    [self.VideoPlayer pause];
    [_Button_Play setHidden:false];//显示播放按钮
}
//分享
-(void)ClickShare
{
    //取消视频预览窗口
    [self.VideoPlayer pause];
    [self dismissViewControllerAnimated:YES completion:nil];
    //分享
    NSString *fullPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/GHJ/Saved/video.mp4"];
    NSArray *activityItemsArray = @[[NSURL fileURLWithPath:fullPath]];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItemsArray applicationActivities:nil];
    activityVC.modalInPopover = NO;
    UIActivityViewControllerCompletionWithItemsHandler itemsBlock = ^(UIActivityType __nullable activityType, BOOL completed, NSArray * __nullable returnedItems, NSError * __nullable activityError){
        if (completed == YES) {
            NSLog(@"completed");
        }else{
            NSLog(@"cancel");
        }
    };
    activityVC.completionWithItemsHandler = itemsBlock;
    UIViewController *rootVC = [UIApplication sharedApplication].delegate.window.rootViewController;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        activityVC.popoverPresentationController.sourceView = rootVC.view;
        activityVC.popoverPresentationController.sourceRect = CGRectMake(rootVC.view.center.x-rootVC.view.frame.size.width * 300/750 * 1/2, rootVC.view.center.y-rootVC.view.frame.size.height * 300/750 * 1/2, rootVC.view.frame.size.width * 300/750, rootVC.view.frame.size.height * 300/750);//尖尖显示的时候是加号，不显示的时候是减号，rootVC.view.center.y+rootVC.view.frame.size.height * 300/750 * 1/2,
        activityVC.popoverPresentationController.delegate = self;
        [activityVC.popoverPresentationController setPermittedArrowDirections:0];//尖尖不显示
        //[activityVC.popoverPresentationController setPermittedArrowDirections: UIPopoverArrowDirectionUp];//尖尖超上
        [rootVC presentViewController:activityVC animated:YES completion:nil];
    } else {
        [rootVC presentViewController:activityVC animated:YES completion:nil];
    }
}
- (BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    return YES;
}
///////////////////////////////////////////////点击事件结束



//预览窗口中的视频播放完调用
-(void)playbackFinished:(NSNotification*)noti
{
    [self.VideoPlayer seekToTime:CMTimeMake(1, 15)];
    [_Button_Play setHidden:false];
}

-(void)delayPauseAction
{

}
//
-(void)PlayUrlFromString:(NSURL *)UrlString setPathString:(NSString *)PathString setfileName:(NSString *)fileName
{
    self.GetfilePathString = PathString;
    self.Getfilename = fileName;
    //视频刚进来时的窗口
    UIButton *someAddButton5 = [UIButton buttonWithType:UIButtonTypeCustom];
    someAddButton5.frame = CGRectMake(0,0, WIDTH, HIGHT);//ipad 预览图片xy位置和大小
    someAddButton5.backgroundColor = [UIColor clearColor];
    [someAddButton5.titleLabel setTextColor:[UIColor clearColor]];
    someAddButton5.titleLabel.backgroundColor = [UIColor clearColor];
    UIImage*tempimeage = [self GetFirstFrameImage:UrlString atTime:1];
    [someAddButton5 setBackgroundImage:tempimeage forState:UIControlStateNormal];
    [someAddButton5 setBackgroundImage:tempimeage forState:UIControlStateSelected];
    [someAddButton5 setBackgroundImage:tempimeage forState:UIControlStateHighlighted];
    [someAddButton5 updateConstraints];
    [someAddButton5 updateFocusIfNeeded];
    [someAddButton5 layoutIfNeeded];
    [self.view addSubview:someAddButton5];
    
    
    //播放时候视频的窗口                                                                START
    AVAsset *movieAsset = [AVURLAsset URLAssetWithURL:UrlString options:nil];
    NSString *pathout = [UrlString absoluteString];
    self.VideoItem = [AVPlayerItem playerItemWithAsset:movieAsset];
    self.VideoPlayer = [AVPlayer playerWithPlayerItem:self.VideoItem];
    self.PlayerLayer = [AVPlayerLayer playerLayerWithPlayer:self.VideoPlayer];
    CGFloat w=floor(WIDTH / 4) * 4+4;
    CGFloat h=floor(HIGHT / 4) * 4+4;
    self.PlayerLayer.frame = CGRectMake(0,0, w, h);//播放时候视频窗口的位置和大小(必须是录制视频分辨率的比例比例错误时 会自动裁剪一部分)
    //[VideoPlayer addTarget:self action:@selector(ClickPause) forControlEvents:UIControlEventTouchUpInside];
    if(_shotModel)
    {
    }
    else
    {
        [self.view.layer addSublayer:self.PlayerLayer];
    }
    
    CGFloat padbutWidth=115*WIDTH/1024*0.5; //ipad  大小
    CGFloat padlinWidth=20;  //ipad 离右边框的间距
    CGFloat padlinBot=20;   //ipad 离顶框的间距
    
    CGFloat iphonebutWidth=55*WIDTH/667;   //iPhone大小
    CGFloat iphonelinWidth=13;   //离右边框的间距
    CGFloat iphonelinBot=7;   //离顶框的间距
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *bundlePath = [bundle resourcePath];
    
    //暂停
    UIButton *someAddButton3 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)//判断是否为ipad
    {
        someAddButton3.frame =CGRectMake(0,0, WIDTH,HIGHT);//按钮位置和大小
    }
    else
    {
        someAddButton3.frame = CGRectMake(0,0,WIDTH,HIGHT);//按钮位置和大小
    }
    bundlePath = [bundle resourcePath];
    someAddButton3.backgroundColor = [UIColor clearColor];//清除button颜色
    [someAddButton3 addTarget:self action:@selector(ClickPause) forControlEvents:UIControlEventTouchUpInside];
    [someAddButton3 updateConstraints];
    [someAddButton3 updateFocusIfNeeded];
    [someAddButton3 layoutIfNeeded];
    [self.view addSubview:someAddButton3];
    
    //取消
    UIButton *someAddButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)//判断是否为ipad
    {
        someAddButton.frame = CGRectMake(WIDTH-padbutWidth-padlinWidth, HIGHT-(HIGHT-padlinBot), padbutWidth, padbutWidth);//按钮位置和大小
    }
    else
    {
        someAddButton.frame = CGRectMake(WIDTH-iphonebutWidth-iphonelinWidth, HIGHT-(HIGHT-iphonelinBot), iphonebutWidth, iphonebutWidth);//按钮位置和大小
    }
    bundlePath = [bundle resourcePath];
    bundlePath = [bundlePath stringByAppendingString:@"/cookeddata/ghj/content/movies/cancel.png"];
    [someAddButton setBackgroundImage:[UIImage imageWithContentsOfFile:bundlePath] forState:UIControlStateNormal];//把图片设置到button上
    [someAddButton addTarget:self action:@selector(ClickCancel) forControlEvents:UIControlEventTouchUpInside];
    [someAddButton updateConstraints];
    [someAddButton updateFocusIfNeeded];
    [someAddButton layoutIfNeeded];
    [self.view addSubview:someAddButton];
    
    //分享
    UIButton *someAddButton2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)//判断是否为ipad
    {
        someAddButton2.frame = CGRectMake(WIDTH-padbutWidth-padlinWidth, HIGHT/2-padbutWidth/2, padbutWidth, padbutWidth);//按钮位置和大小
    }
    else
    {
        someAddButton2.frame = CGRectMake(WIDTH-iphonebutWidth-iphonelinWidth, HIGHT/2-iphonebutWidth/2, iphonebutWidth, iphonebutWidth);//按钮位置和大小
    }
    bundlePath = [bundle resourcePath];
    bundlePath = [bundlePath stringByAppendingString:@"/cookeddata/ghj/content/movies/share.png"];
    [someAddButton2 setBackgroundImage:[UIImage imageWithContentsOfFile:bundlePath] forState:UIControlStateNormal];//把图片设置到button上
    [someAddButton2 addTarget:self action:@selector(ClickShare) forControlEvents:UIControlEventTouchUpInside];
    [someAddButton2 updateConstraints];
    [someAddButton2 updateFocusIfNeeded];
    [someAddButton2 layoutIfNeeded];
    [self.view addSubview:someAddButton2];
   
    //播放
    UIButton *someAddButton1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _Button_Play = someAddButton1;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)//判断是否为ipad
    {
        someAddButton1.frame = CGRectMake(WIDTH/2-padbutWidth/2, HIGHT/2-padbutWidth/2, padbutWidth, padbutWidth);//按钮位置和大小
    }
    else
    {
        someAddButton1.frame = CGRectMake(WIDTH/2-iphonebutWidth/2, HIGHT/2-iphonebutWidth/2, iphonebutWidth, iphonebutWidth);//按钮位置和大小
    }
    bundlePath = [bundle resourcePath];
    bundlePath = [bundlePath stringByAppendingString:@"/cookeddata/ghj/content/movies/play.png"];
    [someAddButton1 setBackgroundImage:[UIImage imageWithContentsOfFile:bundlePath] forState:UIControlStateNormal];//把图片设置到button上
    [someAddButton1 addTarget:self action:@selector(ClickPlay) forControlEvents:UIControlEventTouchUpInside];
    [someAddButton1 updateConstraints];
    [someAddButton1 updateFocusIfNeeded];
    [someAddButton1 layoutIfNeeded];
    [self.view addSubview:someAddButton1];
    
    
   
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playbackFinished:) name:AVPlayerItemDidPlayToEndTimeNotification object:self.VideoPlayer.currentItem];
    [_Button_Play setHidden:false];//显示播放按钮
    [super updateViewConstraints];//刷新页面
}
@end

//
@interface TViewController ()<RPPreviewViewControllerDelegate,RPScreenRecorderDelegate>
{

}
@end

@implementation TViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    //NSLog(@"sffff");
    CGRect frame = CGRectMake(90, 1066, 600, 60);
    UIButton *someAddButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    someAddButton.backgroundColor = [UIColor clearColor];
    [someAddButton setTitle:@"dsdsds!" forState:UIControlStateNormal];
    someAddButton.frame = frame;
    [someAddButton addTarget:self action:@selector(someButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:someAddButton];
}


-(void)someButtonClicked
{
    //NSLog(@"fffffddddddddddd");
}

-(void)Kitstart{
    if ([RPScreenRecorder sharedRecorder].available) {
        //NSLog(@"OK");
        

        [[RPScreenRecorder sharedRecorder] startRecordingWithMicrophoneEnabled:YES handler:^(NSError * _Nullable error) {
            //NSLog(@"%@", error);

        }];
    } else {
        //NSLog(@"dsfsfsdfs");
    }
}

-(void)KitEnd{

    [[RPScreenRecorder sharedRecorder] stopRecordingWithHandler:^(RPPreviewViewController * _Nullable previewViewController, NSError * _Nullable error) {
        if (error) {
            NSLog(@"%@", error);

        }
        if (previewViewController) {
            previewViewController.previewControllerDelegate = self;
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                previewViewController.modalPresentationStyle = UIModalPresentationPopover;
                previewViewController.preferredContentSize =CGSizeMake(1066,600);
                previewViewController.popoverPresentationController.sourceRect= CGRectMake(508, 50, 10, 10);
                previewViewController.popoverPresentationController.sourceView = [IOSAppDelegate GetDelegate].IOSController.view;
            }
             [[IOSAppDelegate GetDelegate].IOSController presentViewController : previewViewController animated : YES completion : nil];
        }
    }];
    
}

- (void)previewControllerDidFinish:(RPPreviewViewController *)previewController {
    [previewController dismissViewControllerAnimated:YES completion:nil];
    
}
-(void)InitReplayKitCaptureActor
{

}

//录屏准备
-(void)ReadyReplayKitCapture:(NSString *)VideoName
{
    CGFloat VideoWidth=  UIScreen.mainScreen.bounds.size.width;
    CGFloat VideoHeight = UIScreen.mainScreen.bounds.size.height;
    NSDictionary *Infos;
    //按设备分辨率录的视频
    CGFloat Resolution_W = [UIScreen mainScreen].scale * WIDTH;
    CGFloat Resolution_H = [UIScreen mainScreen].scale * HIGHT;
    Infos =@{@"AVVideoCodecKey":AVVideoCodecTypeH264,@"AVVideoWidthKey":@(Resolution_W),@"AVVideoHeightKey":@(Resolution_H)};
    _videoInput=[[AVAssetWriterInput alloc]initWithMediaType:AVMediaTypeVideo outputSettings:Infos];
    NSError *Error;
    NSString *VideoPath=[NSString stringWithFormat:@"%@/Documents/GHJ/Saved/%@.mp4",NSHomeDirectory(),VideoName];
    self.filename = VideoName;
    self.filePathString = VideoPath;
    self.filePath = [NSURL fileURLWithPath:VideoPath];
    _assetWriter = [[AVAssetWriter alloc]initWithURL:self.filePath fileType:AVFileTypeMPEG4 error:&Error];
    _videoInput.expectsMediaDataInRealTime = true;
    [_assetWriter addInput:_videoInput];
    [[RPScreenRecorder sharedRecorder] setMicrophoneEnabled:true];
    AudioChannelLayout acl;
    bzero( &acl, sizeof(acl));
    acl.mChannelLayoutTag = kAudioChannelLayoutTag_Mono;
    NSDictionary *audioOutputSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                         [NSNumber numberWithInteger:kAudioFormatMPEG4AAC], AVFormatIDKey,
                                         [ NSNumber numberWithFloat: 44100.0 ], AVSampleRateKey,
                                         [NSNumber numberWithInt:64000], AVEncoderBitRatePerChannelKey,
                                         [ NSData dataWithBytes: &acl length: sizeof( acl ) ], AVChannelLayoutKey,
                                         nil];
    _micInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeAudio outputSettings:audioOutputSettings];
    _micInput.expectsMediaDataInRealTime = true;
    if( [_assetWriter canAddInput:_micInput])
    {
        [_assetWriter addInput:_micInput];
    }
}


//开始录屏
-(void)StartReplayKitCapture
{
    
    [[RPScreenRecorder sharedRecorder]startCaptureWithHandler:^(CMSampleBufferRef  _Nonnull sampleBuffer, RPSampleBufferType bufferType, NSError * _Nullable error) {
        
        if(CMSampleBufferDataIsReady(sampleBuffer))
        {

            if(bufferType == RPSampleBufferTypeVideo)
            {

                if(_assetWriter.status ==AVAssetWriterStatusUnknown)
                {
                    [_assetWriter startWriting];
                    [_assetWriter startSessionAtSourceTime:CMSampleBufferGetPresentationTimeStamp(sampleBuffer)];   
            
                 }
                 
                if([_videoInput isReadyForMoreMediaData])
                {
                        [_videoInput appendSampleBuffer:sampleBuffer];
                }
            }
            if(bufferType == RPSampleBufferTypeAudioMic)
            {

                if([_micInput isReadyForMoreMediaData])
                {
                    [_micInput appendSampleBuffer:sampleBuffer];
                }
            }
        }
        
        
    } completionHandler:^(NSError * _Nullable error) {
        
    }
     
     ];
}


//停止录屏
-(void)StopReplaykitCapture
{
    [_videoInput markAsFinished];
    [_micInput markAsFinished];
    [[RPScreenRecorder sharedRecorder] stopCaptureWithHandler:^(NSError * _Nullable error) {
        [_assetWriter finishWritingWithCompletionHandler:^{
            TVideoPlay *TVPlayer = [[TVideoPlay alloc] init];
            TVPlayer.view.backgroundColor = [UIColor whiteColor];
            NSString * path = [self.filePath absoluteString];
            [TVPlayer PlayUrlFromString:self.filePath setPathString:self.filePathString setfileName:self.filename];
            [[IOSAppDelegate GetDelegate].IOSController presentViewController : TVPlayer animated : YES completion : nil];
        }];
    }];
}

-(void)SavePhotoDone
{
    
}

-(void)SaveVideoToPhoto:(NSString *)VideoName
{
    NSString *VideoPath=[NSString stringWithFormat:@"%@/Documents/GHJ/Saved/%@.mp4",NSHomeDirectory(),VideoName];
    NSURL *FilePath = [NSURL fileURLWithPath:VideoPath];
    
    UISaveVideoAtPathToSavedPhotosAlbum(VideoPath, nil, nil, nil);
}

@end


#endif


bool AMyActor::IsImagePathFromA()
{
    bool value = 0;
#if PLATFORM_IOS
    value = [ScreenshotEncoder IsImagePathFromA];
#endif
    return value;
}

bool AMyActor::IsImagePathFromB()
{
    bool value = 0;
#if PLATFORM_IOS
    value = [ScreenshotEncoder IsImagePathFromB];
#endif
    return value;
}

FString AMyActor::GetTestPathUE()
{
    
FString filename = "image.png";
#if PLATFORM_IOS
    const char *IOSFilePath = [ScreenshotEncoder GetFilePath];
      filename = IOSFilePath;
#endif
    
    
    return filename;
}


bool AMyActor::IsScreenshotDone(FString savePath)
{
	return FPaths::FileExists(*savePath);
}

string AMyActor::FStringCasetosting(FString IFString)
{
	std::string HostString(TCHAR_TO_UTF8(*IFString));
	return HostString;
}


bool AMyActor::RequestNewScreenshot(FString IFString)
{
	string CasetoString = FStringCasetosting(IFString);
	bool isthes = 1;
#if PLATFORM_IOS
	NSString *astring = [NSString stringWithCString : CasetoString.c_str() encoding : [NSString defaultCStringEncoding]];
	isthes = [ScreenshotEncoder showItemInTmall4iOS : astring];
#endif
	return isthes;
}

//保存图片到相册
void AMyActor::SaveNativeScreenshot(FString IFString)
{
	string CasetoString = FStringCasetosting(IFString);


#if PLATFORM_IOS
	NSString *astring = [NSString stringWithCString : CasetoString.c_str() encoding : [NSString defaultCStringEncoding]];
	[ScreenshotEncoder saveShot : astring];
#endif
}

// Sets default values
AMyActor::AMyActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMyActor::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AMyActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMyActor::Base(FString &ProjectPersistentDownloadDir, FString &ProjectContentDir)
{
	ProjectContentDir = FPaths::ProjectContentDir();
	ProjectPersistentDownloadDir = FPaths::ProjectPersistentDownloadDir();
#if PLATFORM_IOS
    
#endif
}





void AMyActor::GetAllPaths(TArray<FString>& Files, const FString & FilePath, const FString& Extension)
{
	FString SearchedFiles = FilePath + Extension;
	TArray<FString> FindedFiles;

	IFileManager::Get().FindFiles(FindedFiles, *SearchedFiles, true, false);

	FString SearchFile = "";

	for (int i = 0; i < FindedFiles.Num(); i++)
	{
		SearchFile = FilePath + FindedFiles[i];
		Files.Add(SearchFile);

	}

}


UObject* AMyActor::LoadPakFile(TArray<FString> &FileList, FString OUTLOG, FString MPoint, FString GetObjectPathFile)
{
	IPlatformFile& PlatfromFile = FPlatformFileManager::Get().GetPlatformFile();

	FPakPlatformFile* PakPlatfromFile = new FPakPlatformFile();
	PakPlatfromFile->Initialize(&PlatfromFile, TEXT(""));
	FPlatformFileManager::Get().SetPlatformFile(*PakPlatfromFile);
	const FString PakFilename = OUTLOG;
	FPakFile PakFile(&PlatfromFile, *PakFilename, false);
	FString MountPoint(FPaths::GameContentDir());
	MountPoint = MPoint;
	PakFile.SetMountPoint(*MountPoint);

	if (PakPlatfromFile->Mount(*PakFilename, 0, *MountPoint))
	{
		//GEngine->AddOnScreenDebugMessage(-1, 20.0, FColor::Red, TEXT("Mount Success"));
		PakFile.FindFilesAtPath(FileList, *PakFile.GetMountPoint(), true, false, true);
		FStreamableManager StreamableManager;
		FString AssetName = FileList[0];
		FString MackPath = TEXT("/Game/") + GetObjectPathFile;
		FString AssetShortName = FPackageName::GetShortFName(MackPath).ToString();
		FString LeftStr;
		FString RightStr;
		AssetShortName.Split(TEXT("."), &LeftStr, &RightStr);
		FString AFName = LeftStr;
		MackPath.Split(TEXT("."), &LeftStr, &RightStr);
		AssetName = LeftStr + TEXT(".") + AFName;
		FStringAssetReference Reference = AssetName;
		//GEngine->AddOnScreenDebugMessage(-1, 20.0, FColor::Red, AssetName);
		UObject *LoadObjects = StreamableManager.SynchronousLoad(Reference);
		if (LoadObjects != nullptr)
		{
			//UE_LOG(LogClass, Log, TEXT("Object Load Success..."));
			//GEngine->AddOnScreenDebugMessage(-1, 20.0, FColor::Red, TEXT("Object Load Success..."));
			return LoadObjects;
		}
		else
		{
			//UE_LOG(LogClass, Log, TEXT("Can not Load Asset..."));
			//GEngine->AddOnScreenDebugMessage(-1, 20.0, FColor::Red, TEXT("Can not Load Asset..."));
            return LoadObjects;
		}

	}
	else
	{
		//UE_LOG(LogClass, Error, TEXT("Mount Failed"));
		return nullptr;
	}



}

//CleanPath(FString Path)
void AMyActor::TCleanFile(FString Path)
{
    IPlatformFile& PlatfromFile = FPlatformFileManager::Get().GetPlatformFile();
    PlatfromFile.DeleteFile(*Path);
}

FString AMyActor::GetLastShotPath()
{
	return FScreenshotRequest::GetFilename();
}

void AMyActor::ShareImage()
{
#if PLATFORM_IOS
    [ScreenshotEncoder addActivityViewController];
#endif
}

/*
fix
float AMyActor::GetARFloats(FAppleARKitFrame Current)
{

	return Current.LightEstimate.AmbientIntensity;
}
*/

static EImageFormat GetJoyImageFormat(EJoyImageFormats JoyFormat)
{
	switch (JoyFormat)
	{
	case EJoyImageFormats::JPG: return EImageFormat::JPEG;
	case EJoyImageFormats::PNG: return EImageFormat::PNG;
	case EJoyImageFormats::BMP: return EImageFormat::BMP;
	case EJoyImageFormats::ICO: return EImageFormat::ICO;
	case EJoyImageFormats::EXR: return EImageFormat::EXR;
	case EJoyImageFormats::ICNS: return EImageFormat::ICNS;
	}
	return EImageFormat::JPEG;
}


static void WriteRawToTexture_RenderThread(FTexture2DDynamicResource* TextureResource, const TArray<uint8>& RawData, bool bUseSRGB = true)
{
	check(IsInRenderingThread());

	FTexture2DRHIParamRef TextureRHI = TextureResource->GetTexture2DRHI();

	int32 Width = TextureRHI->GetSizeX();
	int32 Height = TextureRHI->GetSizeY();

	uint32 DestStride = 0;
	uint8* DestData = reinterpret_cast<uint8*>(RHILockTexture2D(TextureRHI, 0, RLM_WriteOnly, DestStride, false, false));

	for (int32 y = 0; y < Height; y++)
	{
		uint8* DestPtr = &DestData[(Height - 1 - y) * DestStride];

		const FColor* SrcPtr = &((FColor*)(RawData.GetData()))[(Height - 1 - y) * Width];
		for (int32 x = 0; x < Width; x++)
		{
			*DestPtr++ = SrcPtr->B;
			*DestPtr++ = SrcPtr->G;
			*DestPtr++ = SrcPtr->R;
			*DestPtr++ = SrcPtr->A;
			SrcPtr++;
		}
	}

	RHIUnlockTexture2D(TextureRHI, 0, false, false);
}


UTexture2DDynamic* AMyActor::Victory_LoadTexture2D_FromFile(const FString& FullFilePath, EJoyImageFormats ImageFormat, bool& IsValid, int32& Width, int32& Height)
{
	IsValid = false;
	UTexture2D* LoadedT2D = NULL;

	IImageWrapperModule& ImageWrapperModule = FModuleManager::LoadModuleChecked<IImageWrapperModule>(FName("ImageWrapper"));

	IImageWrapperPtr ImageWrapper = ImageWrapperModule.CreateImageWrapper(GetJoyImageFormat(ImageFormat));

	//Load From File
	TArray<uint8> RawFileData;
	if (!FFileHelper::LoadFileToArray(RawFileData, *FullFilePath))
	{
		//GEngine->AddOnScreenDebugMessage(-1, 20.0, FColor::Red, TEXT("Error : Image LoadFileToArray Is Falid"));
		return NULL;
	}
	
	//GEngine->AddOnScreenDebugMessage(-1, 20.0, FColor::Green, TEXT("Image LoadFileToArray Is Succee"));
	if (ImageWrapper.IsValid() && ImageWrapper->SetCompressed(RawFileData.GetData(), RawFileData.Num()))
	{


		const TArray<uint8>* RawData = NULL;
		if (ImageWrapper->GetRaw(ERGBFormat::BGRA, 8, RawData))
		{
			if (UTexture2DDynamic* Texture = UTexture2DDynamic::Create(ImageWrapper->GetWidth(), ImageWrapper->GetHeight()))
			{
				Texture->SRGB = true;
				Texture->UpdateResource();

				ENQUEUE_UNIQUE_RENDER_COMMAND_TWOPARAMETER(
					FWriteRawDataToTexture,
					FTexture2DDynamicResource*, TextureResource, static_cast<FTexture2DDynamicResource*>(Texture->Resource),
					TArray<uint8>, RawData, *RawData,
					{
						WriteRawToTexture_RenderThread(TextureResource, RawData);
					});

				//OnSuccess.Broadcast(Texture);
				return Texture;
			}
		}
	}

	return NULL;
}

bool AMyActor::RegWeixin()
{
    bool result = 0;
#if PLATFORM_IOS
    result =  [ScreenshotEncoder RegWeixinTOB];
#endif
    return result;
}

void AMyActor::WeixinShareText()
{
#if PLATFORM_IOS
    [ScreenshotEncoder ShareTextTOB];
#endif
}

/////////////////////////////////
bool AMyActor::WeixinShareImage(FString InImagename)
{
    bool result = 0;
#if PLATFORM_IOS
    string CasetoString = FStringCasetosting(InImagename);
    NSString *astring = [NSString stringWithCString : CasetoString.c_str() encoding : [NSString defaultCStringEncoding]];
    result = [ScreenshotEncoder ShareImageTOB:astring];
#endif
    return result;
}
bool AMyActor::WeixinShareImagePengyou(FString InImagename)
{
    bool result = 0;
#if PLATFORM_IOS
    string CasetoString = FStringCasetosting(InImagename);
    NSString *astring = [NSString stringWithCString : CasetoString.c_str() encoding : [NSString defaultCStringEncoding]];
    result = [ScreenshotEncoder ShareImageTOBPengyou:astring];
#endif
    return result;
}
bool AMyActor::WeixinShareImageShouchang(FString InImagename)
{
    bool result = 0;
#if PLATFORM_IOS
    string CasetoString = FStringCasetosting(InImagename);
    NSString *astring = [NSString stringWithCString : CasetoString.c_str() encoding : [NSString defaultCStringEncoding]];
    result = [ScreenshotEncoder ShareImageTOBShouchang:astring];
#endif
    return result;
}

//////////////////////////////////////////////////////////

bool AMyActor::WeixinShareVideo(FString InVideoname)
{
    bool result = 0;
#if PLATFORM_IOS
    string CasetoString = FStringCasetosting(InVideoname);
    NSString *astring = [NSString stringWithCString : CasetoString.c_str() encoding : [NSString defaultCStringEncoding]];
    result = [ScreenshotEncoder ShareVideoTOBHaoyou:astring];
#endif
    return result;
}
bool AMyActor::WeixinShareVideoPengyou(FString InVideoname)
{
    bool result = 0;
#if PLATFORM_IOS
    string CasetoString = FStringCasetosting(InVideoname);
    NSString *astring = [NSString stringWithCString : CasetoString.c_str() encoding : [NSString defaultCStringEncoding]];
    result = [ScreenshotEncoder ShareVideoTOBPengYou:astring];
#endif
    return result;
}
bool AMyActor::WeixinShareVideoShouchang(FString InVideoname)
{
    bool result = 0;
#if PLATFORM_IOS
    string CasetoString = FStringCasetosting(InVideoname);
    NSString *astring = [NSString stringWithCString : CasetoString.c_str() encoding : [NSString defaultCStringEncoding]];
    result = [ScreenshotEncoder ShareVideoTOBShouchang:astring];
#endif
    return result;
}



//////////////////////////////////////////

////////////////////////////////////////////////////////////




bool AMyActor::isWeixinCanOpen()
{
    bool result = 0;
#if PLATFORM_IOS
    result = [AlphaLibrary isInstalledWeixin];
#endif
    return result;
}

bool AMyActor::TRecordStart()
{
    bool result = 1;
#if PLATFORM_IOS
    [TVC Kitstart];
#endif
    return result;
}
bool AMyActor::TRecordEnd()
{
    bool result = 1;
#if PLATFORM_IOS
    [TVC KitEnd];
#endif
    return result;
}



//初始化录屏
void AMyActor::InitCapture()
{
#if PLATFORM_IOS
    TVC = [[TViewController alloc] init];
    [TVC InitReplayKitCaptureActor];
#endif
}
//准备录制视频
void AMyActor::ReadyCapture(FString Videoname)
{
#if PLATFORM_IOS
    string CasetoString = FStringCasetosting(Videoname);
    NSString *astring = [NSString stringWithCString : CasetoString.c_str() encoding : [NSString defaultCStringEncoding]];
    [TVC ReadyReplayKitCapture:astring];
#endif
}
//开始录制视频
void AMyActor::StartCapture()
{
#if PLATFORM_IOS
    [TVC StartReplayKitCapture];
#endif
}
//停止录制视频
void AMyActor::StopCapture()
{
#if PLATFORM_IOS
    [TVC StopReplaykitCapture];
#endif
}
//保存视频到相册
void AMyActor::SaveVideoToPhotoFunc(FString InVideoname)
{
#if PLATFORM_IOS
    string CasetoString = FStringCasetosting(InVideoname);
    NSString *astring = [NSString stringWithCString : CasetoString.c_str() encoding : [NSString defaultCStringEncoding]];
    [TVC SaveVideoToPhoto:astring];
#endif
}

//判断是否为ipad
bool AMyActor::isIpad()
{
    bool result = 0;
#if PLATFORM_IOS
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)//判断是否为ipad
    {
        result = 1;
    }
    else
    {
        result = 0;
    }
#endif
    return result;
}
